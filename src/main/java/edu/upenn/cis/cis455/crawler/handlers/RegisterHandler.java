package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegisterHandler implements Route {
    StorageInterface db;
    
    public RegisterHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
        String first = req.queryParams("firstName");
        String last = req.queryParams("lastName");
        
        System.err.println("Login request for " + user + " and " + pass);
        int id = db.addUser(user, pass, first, last);
        Session session = req.session();
        session.attribute("user", user);
        session.attribute("password", pass);
        resp.redirect("/index.html");
        return "";
    }
}
