package edu.upenn.cis.cis455.crawler;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;


import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Level;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis.cis455.crawler.info.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.info.URLInfo;
import edu.upenn.cis.cis455.crawler.CrawlerWorker;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;


public class Crawler implements CrawlMaster {
    final static Logger logger = LogManager.getLogger(Crawler.class);
    static final int NUM_WORKERS = 5;
    
    private String startURL;
    private StorageInterface db;
    private int size;
    private int count;
    private int crawled;
    private BlockingQueue<String> urlQueue = new LinkedBlockingQueue<>();
    private Map<String, List<String>> urlMap = new ConcurrentHashMap<>();
    private List<CrawlerWorker> workers;
    private Set<String> contentSeen = new HashSet<>();
    private Set<String> urlSeen = new HashSet<>();
    private Map<String, RobotsTxtInfo> robots = new HashMap<>();
    private Map<String, Long> lastVisited = new HashMap<>();
    
    int busy = 0;
    int shutdown = 0;
    public void addContentSeen(String s) {
        contentSeen.add(s);
    }
    
    public void addUrlSeen(String s) {
        urlSeen.add(s);
    }
    
    public boolean seenUrl(String s) {
        return urlSeen.contains(s);
    }
    
    public void setLastVisited(String site) {
        lastVisited.put(site, System.currentTimeMillis());
    }
    
    public boolean seenContent(String s) {
        return contentSeen.contains(s);
    }
    
    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
        this.startURL = startUrl;
        this.db = db;
        this.size = size;
        this.count = count;
        this.workers = new ArrayList<>();
        this.urlQueue = new LinkedBlockingQueue<>();
        this.urlMap = new HashMap<>();
    }

    ///// TODO: you'll need to flesh all of this out.  You'll need to build a thread
    // pool of CrawlerWorkers etc. and to implement the functions below which are
    // stubs to compile
    
    /**
     * Main thread
     */
    public void start() {
        URLInfo info = new URLInfo(this.startURL);
        urlMap.put(info.getHostName(), new ArrayList<>());
        urlMap.get(info.getHostName()).add(startURL);
        urlQueue.add(info.getHostName());
        for (int i = 0; i < NUM_WORKERS; i++) {
            CrawlerWorker worker = new CrawlerWorker(db, urlQueue, urlMap, this);
            workers.add(worker);
            worker.start();
        }
    }
    
    /**
     * Returns true if it's permissible to access the site right now
     * eg due to robots, etc.
     */
    public boolean isOKtoCrawl(String site, int port, boolean isSecure) {
        if (!robots.containsKey(site)) {
            setLastVisited(site);
            String urlString = (isSecure ? "https://" : "http://") + site + ((port != 80) ? ":" + port : "") + "/robots.txt";
            try {
                URL url = new URL(urlString);
                URLInfo info = new URLInfo(urlString);
                InputStream stream = downloadPage(info, url);
                RobotsTxtInfo robot = new RobotsTxtInfo();
                String line;
                BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
                try {
                    String userAgent = "";
                    while ((line = rd.readLine()) != null) {
                        if (line.startsWith("Location:")) {
                            urlString = line.replace("Location: ", "");
                            rd = new BufferedReader(new InputStreamReader(downloadPage(new URLInfo(urlString), new URL(urlString))));
                        } else if (line.startsWith("User-agent:")) {
                            userAgent = line.replace("User-agent: ", "");
                            robot.addUserAgent(userAgent);
                        } else if (userAgent.equals("*") || userAgent.equals("cis455crawler")) {
                            if (line.startsWith("Disallow:")) {
                                robot.addDisallowedLink(userAgent, line.replace("Disallow: ", ""));
                            } else if (line.startsWith("Allow:")) {
                                robot.addAllowedLink(userAgent, line.replace("Allow: ", ""));
                            } else if (line.startsWith("Crawl-delay:")) {
                                robot.addCrawlDelay(userAgent, Integer.valueOf(line.replace("Crawl-delay: ", "")));
                            } else if (line.startsWith("Sitemap:")) {
                                robot.addSitemapLink(line.replace("Sitemap: ", ""));
                            }
                        }
                    }
                    rd.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                robots.put(site, robot);
            } catch (Exception e) {

            }
        }
        if (robots.get(site) == null) {
            return true;
        } else {
            RobotsTxtInfo robTmp = robots.get(site);
            if (!((robTmp.getDisallowedLinks("*") != null && robTmp.getDisallowedLinks("*").contains("/")) || (robTmp.getDisallowedLinks("cis455crawler") != null && robTmp.getDisallowedLinks("cis455crawler").contains("/"))))
                return true;
            if (robTmp.getAllowedLinks("cis455crawler") != null && robTmp.getAllowedLinks("cis455crawler").contains("/"))
                return true;
            return false;
        }
    }
    
    public InputStream downloadPage(URLInfo info, URL url) throws Exception{
        logger.info(info.toString() + ": Downloading");
        InputStream stream;
        if (info.isSecure()) {
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("User-Agent", "cis455crawler");
            stream = connection.getInputStream();
        } else {
            HttpURLConnection connection = (HttpsURLConnection)url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("User-Agent", "cis455crawler");
            stream = connection.getInputStream();
        }
        return stream;
    }
    /**
     * Returns true if the crawl delay says we should wait
     */
    public boolean deferCrawl(String site) {
        RobotsTxtInfo robot = robots.get(site);
        if (robot == null)
            return false;
        int defer = robot.getCrawlDelay("cis455crawler");
        if (defer < 0)
            defer = robot.getCrawlDelay("*");
        Long last = lastVisited.get(site);
        if (last == null) {
            return false;
        }
        if (defer < 0)
            return false;
        return 1000 * defer > System.currentTimeMillis() - last; 
    }
    
    /**
     * Returns true if it's permissible to fetch the content,
     * eg that it satisfies the path restrictions from robots.txt
     */
    public boolean isOKtoParse(String site, URLInfo url) {
        if (seenUrl(url.toString()))
            return false;
        String contentType = "";
        int contentLength = 0;
        String response = sendRequest(url, "HEAD");
        if (response.toLowerCase().startsWith("http/1.1 30")) {
            String newLocation = "";
            for(String line : response.split("\r\n")) {
                if (line.toLowerCase().startsWith("location")) {
                    newLocation = line.split(" ")[1];
                    break;
                }
            }
            URLInfo redirectUrlInfo = new URLInfo(newLocation);
            synchronized (this){
                response = sendRequest(redirectUrlInfo, "HEAD");
            }
        }

        for(String line : response.split("\r\n")) {
            if (line.toLowerCase().startsWith("content-type")) {
                contentType = line.split(" ")[1];
            }
            if(line.toLowerCase().startsWith("content-length")){
                contentLength = Integer.parseInt(line.split(" ")[1]);
            }
        }
        if(contentLength > this.size * 1000000)
            return false;
        return true;
    }
    
    public String sendRequest(URLInfo info, String method) {
        String result = "";
        try {
            PrintWriter out;
            InputStream in;
            BufferedReader reader;
            if (info.isSecure()) {
                URL url = new URL(info.toString());
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setInstanceFollowRedirects(true);
                conn.setRequestProperty("User-Agent", "cis455crawler");
                conn.setRequestMethod(method);
                conn.setDoOutput(true);
                out = new PrintWriter(conn.getOutputStream());
                in = conn.getInputStream();
                reader = new BufferedReader(new InputStreamReader(in));
            } else {
                URL url = new URL(info.toString());
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setInstanceFollowRedirects(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("User-Agent", "cis455crawler");
                conn.setRequestMethod(method);
                in = conn.getInputStream();
                out = new PrintWriter(conn.getOutputStream());
                reader = new BufferedReader(new InputStreamReader(in));
            }
            /*Socket socket = new Socket(info.toString(), 80);
            logger.debug("send request 2");
            InputStream in = socket.getInputStream();
            logger.debug("send request 3");
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            logger.debug("send request 4");
            PrintWriter out = new PrintWriter(socket.getOutputStream());*/
            StringBuilder req = new StringBuilder();
            req.append(method + " ");
            req.append(info.getFilePath());
            req.append(" HTTP/1.1\r\n");
            req.append("Host: ");

            req.append(info.getHostName());
            req.append(":80 \r\n");
            req.append("Connection: close\r\n");
            req.append("\r\n");

            out.write(req.toString());

            out.flush();
            String line;
            while ((line = reader.readLine()) != null) {
                result += (line + "\r\n");
            }

            reader.close();
        } catch (Exception e) {

        }
        return result;
    }
    
    /**
     * Returns true if the document content looks worthy of indexing,
     * eg that it doesn't have a known signature
     */
    public boolean isIndexable(String content, URLInfo info) {
        ArrayList<String> disallowed = robots.get(info.getHostName()).getDisallowedLinks("cis455crawler");
        if (disallowed == null) {
            disallowed = robots.get(info.getHostName()).getDisallowedLinks("*");
        }
        if (disallowed == null || disallowed.contains(info.getFilePath())) {
           synchronized(contentSeen) {
            return !seenContent(content);
            } 
        } else
            return false;
    }
    
    /**
     * We've indexed another document
     */
    public void incCount() {
        crawled++;
    }
    
    /**
     * Workers can poll this to see if they should exit, ie the
     * crawl is done
     */
    public boolean isDone() {
        return crawled >= count || (busy == 0 && urlQueue.isEmpty());
    }
    
    /**
     * Workers should notify when they are processing an URL
     */
    public void setWorking(boolean working) {
        if (working) {
            busy++;
        } else {
            busy--;
        }
    }
    
    /**
     * Workers should call this when they exit, so the master
     * knows when it can shut down
     */
    public void notifyThreadExited() {
        synchronized(this) {
            shutdown++;
        }
    }
    
    public void waitThread() {
        while (shutdown < NUM_WORKERS) {
            try {
                Thread.sleep(10);
            } catch(Exception ie) {
            }
        }
    }
    /**
     * Main program:  init database, start crawler, wait
     * for it to notify that it is done, then close.
     */
    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        
        if (args.length < 3 || args.length > 5) {
            System.exit(1);
        }
        
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;
        
        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);
        
        logger.info("Starting crawl of " + count + " documents, starting at " + startUrl + "\n");
        crawler.start();
        
        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        // TODO: final shutdown
        crawler.waitThread();
        db.close();
    }

}
