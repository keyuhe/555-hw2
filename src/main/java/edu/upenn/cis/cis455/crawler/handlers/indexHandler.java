package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;

public class indexHandler implements Route {
    public indexHandler() {
        
    }
    
    @Override
    public String handle(Request request, Response response) throws HaltException {
        String s = "<html><head><title>CIS 555</title></head><body><h1>CIS HW2</h1>Welcome, ";
        s += request.attribute("user");
        s += "<ul><li><a href='/login-form.html'>Log in as a different user</a><li><a href='/register.html'>Register a user</a><li><a href='/logout'>Log out</a></ul></body></html>";
        
        response.type("text/html");
        return s;
    }
}
