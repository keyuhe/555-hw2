package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class LookUpHandler implements Route {
    private StorageInterface db;
    
    public LookUpHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request request, Response response) throws HaltException {
        response.type("text/html");
        String doc;
        System.out.println("url " + request.queryParams("url"));
        try {
            doc = db.getDocument(request.queryParams("url"));
        }
        catch(Exception e) {
            doc = "Not Found";
        }
        System.out.println(doc);
        return doc;
    }
}
