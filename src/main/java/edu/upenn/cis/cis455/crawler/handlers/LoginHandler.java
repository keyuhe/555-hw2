package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class LoginHandler implements Route {
    private StorageInterface db;
    
    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request request, Response response) throws HaltException {
        String user = request.queryParams("username");
        String pass = request.queryParams("password");
        //String sha256Pass = DigestUtils.sha256Hex(pass);
        
        if (db.getSessionForUser(user, pass)) {
            Session session = request.session();
            
            session.attribute("user", user);
            session.attribute("password", pass);
            session.maxInactiveInterval(300);
            response.redirect("/index.html");
        } else {
            response.redirect("/login-form.html");
        }
        return "";
    }
}
