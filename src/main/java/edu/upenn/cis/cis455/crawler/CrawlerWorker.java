package edu.upenn.cis.cis455.crawler;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import javax.xml.bind.DatatypeConverter;

import edu.upenn.cis.cis455.crawler.info.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;

public class CrawlerWorker extends Thread {
    final static Logger logger = LogManager.getLogger(CrawlerWorker.class);

    BlockingQueue<String> urlQueue;
    Map<String,List<String>> urlMap;
    Crawler crawler;
    StorageInterface db;
    
    public CrawlerWorker(StorageInterface db, BlockingQueue<String> queue, Map<String,List<String>> map, Crawler crawler) {
        //setDaemon(true);
        this.db = db;
        this.urlQueue = queue;
        this.urlMap = map;
        this.crawler = crawler;
    }
    
    public void run() {
        while (!crawler.isDone()) {
            try {
                String site = urlQueue.take();
                System.out.println("Thread ID is " + getId() + " site is " + site);
                if (site != null) {
                    crawler.setWorking(true);
                    URLInfo info = null;
                    do {
                        //String host = site.startsWith("http://") ? site.substring(7) : site.substring(8);
                        List<String> urlsFromSite = urlMap.get(site);
                        if (urlsFromSite != null && !urlsFromSite.isEmpty()) {
                            info = new URLInfo(urlsFromSite.remove(0));
                            if(urlsFromSite.isEmpty()) {
                                urlQueue.remove(site);
                                break;
                            }
                            if (crawler.isOKtoCrawl(site, info.getPortNo(), info.isSecure())) {
                                if (crawler.deferCrawl(site)) {
                                    urlsFromSite.add(info.toString());
                                    urlQueue.add(site);
                                } else if (crawler.isOKtoParse(site, info)) {
                                    if (!urlsFromSite.isEmpty())
                                            urlQueue.add(site);
                                    else
                                        urlMap.remove(site);
                                    break;
                                } else {
                                    System.out.println("else");
                                }
                            }
                        }
                    }  while (!crawler.isDone());
                    if (info != null && crawler.isOKtoParse(site, info) ) {
                        parseUrl(info);
                        crawler.addUrlSeen(info.toString());
                    }
                    crawler.setWorking(false);
                }
            } catch (InterruptedException ie) {
                logger.debug("worker error");
            }

        } //while (!crawler.isDone());
        crawler.notifyThreadExited();
    }
    
    public void parseUrl(URLInfo info) {
        try {
            URL url = new URL(info.toString());
            
            InputStream stream = null;
            
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "cis455crawler");
            stream = connection.getInputStream();

            BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                addLinks(info, line);
                response.append('\n');
            }
            rd.close();
            if (crawler.isIndexable(SHA(response.toString())))
                indexText(url.toString(), response.toString());
        } catch (Exception mfe) {
            logger.debug("error in parse");
        } 
    }
    
    void addLinks(URLInfo info, String line) {
        String txt = line;//.toLowerCase();
        
        int href = txt.toLowerCase().indexOf("href");
        while (href >= 0 && txt.length() > 0 && href < txt.length()) {
            href += 4;
            
            boolean foundEquals = false;
            while (href < txt.length() &&(txt.charAt(href) == ' ' || txt.charAt(href) == '\t' || txt.charAt(href) == '=')) {
                foundEquals = (txt.charAt(href) == '=');
                href++;
            }

                
            if (foundEquals && 
            href >= 0 && href < txt.length()) {
                char quote = txt.charAt(href);

                // HREF
                if (quote == '\'' || quote == '\"') {
                    int end = txt.indexOf(quote, href+1);
                    if (end >= href) {
                        enqueueLink(info, txt.substring(href+1, end));
                    }
               }
            } 
            txt = txt.substring(href);
            href = txt.toLowerCase().indexOf("href");
        }
    }
    
    void enqueueLink(URLInfo info, String link) {
	   if (link.startsWith("/")) {
			String nextUrl = (info.isSecure() ? "https://" : "http://") + info.getHostName()
					+ (info.getPortNo() == 80 ? "" : ":" + info.getPortNo()) + link;

			addToQueue(nextUrl);
		} else if (link.startsWith("http://") || link.startsWith("https://")) {

			if (link.contains(info.getHostName())) {
				addToQueue(link);
			} else {
				addToQueue(link);
			}
		} else {
			String nextUrl = "";
			nextUrl += ((info.isSecure() ? "https://" : "http://") + info.getHostName()
					+ (info.getPortNo() == 80 ? "" : ":" + info.getPortNo()) + info.getFilePath());
			if (nextUrl.endsWith("/"))
				nextUrl += link;
			else
				nextUrl = (nextUrl.substring(0, nextUrl.lastIndexOf('/') + 1) + link);
			addToQueue(nextUrl);
		}
	}
	
	synchronized void addToQueue(String nextUrl) {        
        URLInfo info = new URLInfo(nextUrl);
        if (!urlMap.containsKey(info.getHostName())) {
            urlMap.put(info.getHostName(), new ArrayList<>());
        }

        urlMap.get(info.getHostName()).add(nextUrl);
        if (!urlQueue.contains(info.getHostName())) {
            urlQueue.add(info.getHostName());
        }
    }
    
    public void indexText(String url, String line) {
        crawler.incCount();
        System.out.println("index: " + url);
        synchronized(crawler) {
            crawler.addContentSeen(SHA(line));
        }
        db.addDocument(url, line);
    }
    
    public String SHA(String s){
        byte[] result = {};
        String strHash = "hash";
        try {
            MessageDigest sha = MessageDigest.getInstance("MD5");
            result = sha.digest(s.getBytes(StandardCharsets.UTF_8));
            strHash = DatatypeConverter.printHexBinary(result);
        }
        catch(java.security.NoSuchAlgorithmException e) {
            logger.debug("no such hash algorithmn");
        }
        return strHash;
    }
}
