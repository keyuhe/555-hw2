package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.EntryBinding;
import com.sleepycat.collections.StoredSortedMap;

import edu.upenn.cis.cis455.model.Corpus;
import edu.upenn.cis.cis455.model.Url;
import edu.upenn.cis.cis455.model.Lexion;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import edu.upenn.cis.cis455.model.User;

public class DBStorage implements StorageInterface {
    
    private Environment env;
    private StoredClassCatalog javaCatalog;
    
    private static final String CLASS_CATALOG = "java_class_catalog";
    private static final String USER_STORE = "user_store";
    private static final String OCCURENCE_EVENT_STORE = "occurence_event_store";
    private static final String CORPUS_STORE = "corpus_store";
    private static final String URL_STORE = "url_store";
    private static final String INV_STORE = "inv_store";
    private static final String LEX_STORE = "lex_store";
    private static final String INV_LEX_STORE = "inv_lex_store";
    
    private Database userDb;
    private Database corpusDb;
    private Database urlsDb;
    private Database invDb;
    private Database lexDb;
    private Database invLexDb;
    
    private StoredSortedMap<String, User> user;
    private StoredSortedMap<Integer, Corpus> corpus;
    private StoredSortedMap<Integer, Url> urls;
    private StoredSortedMap<String, Integer> invUrls;
    private StoredSortedMap<Integer, Lexion> lex;
    private StoredSortedMap<String, Integer> invLex;
    
    public DBStorage(String homeDirectory) {
        File file = new File(homeDirectory);
        if (!file.exists()) {
            file.mkdir();
        }
        
        EnvironmentConfig envConfig = new EnvironmentConfig();
        //create an environment where transactional (and non-transactional) databases may be opened
        envConfig.setTransactional(true);
        // specify that the environment's files will be created if they don't already exist
        envConfig.setAllowCreate(true);
        this.env = new Environment(new File(homeDirectory), envConfig);
        
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, dbConfig);
        
        javaCatalog = new StoredClassCatalog(catalogDb);
        
        userDb = env.openDatabase(null, USER_STORE, dbConfig);
        corpusDb = env.openDatabase(null, CORPUS_STORE, dbConfig);
        urlsDb = env.openDatabase(null, URL_STORE, dbConfig);
        invDb = env.openDatabase(null, INV_STORE, dbConfig);
        lexDb = env.openDatabase(null, LEX_STORE, dbConfig);
        invLexDb = env.openDatabase(null, INV_LEX_STORE, dbConfig);
        
        EntryBinding<String> stringBinding = new StringBinding();
        EntryBinding<Integer> intBinding = new IntegerBinding();
        EntryBinding<User> userBinding = new SerialBinding<User>(javaCatalog, User.class);
        EntryBinding<Corpus> corpusBinding = new SerialBinding<Corpus>(javaCatalog, Corpus.class);
        EntryBinding<Lexion> lexBinding = new SerialBinding<Lexion>(javaCatalog, Lexion.class);
        EntryBinding<Url> urlBinding = new SerialBinding<Url>(javaCatalog, Url.class);
        
	    user = new StoredSortedMap<String,User>(userDb, stringBinding, userBinding, true);
        corpus = new StoredSortedMap<Integer,Corpus>(corpusDb, intBinding, corpusBinding, true);
        urls = new StoredSortedMap<Integer,Url>(urlsDb, intBinding, urlBinding, true);
        invUrls = new StoredSortedMap<String,Integer>(invDb, stringBinding, intBinding, true);
        lex = new StoredSortedMap<Integer,Lexion>(lexDb, intBinding, lexBinding, true);
        invLex = new StoredSortedMap<String,Integer>(lexDb, stringBinding, intBinding, true);
    }
    
    /**
     * How many documents so far?
     */
	public int getCorpusSize() {
	    return corpus.size();
	}
	
	/**
	 * Add a new document, getting its ID
	 */
	public int addDocument(String url, String documentContents) {
	    int id;
	    synchronized(corpus) {
	        id = getCorpusSize();
	        ZonedDateTime dateTime = ZonedDateTime.now();
            String date = dateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME);
            
            corpus.put(id, new Corpus(id, documentContents));
            urls.put(id, new Url(id, url, date));
            invUrls.put(url, id);
	    }
	    return id;
	}
	
	/**
	 * How many keywords so far?
	 */
	public int getLexiconSize() {
	    return lex.size();
	}
	
	/**
	 * Gets the ID of a word (adding a new ID if this is a new word)
	 */
	public int addOrGetKeywordId(String keyword) {
	    if (invLex.containsKey(keyword))
	       return invLex.get(keyword);
	    int id = getLexiconSize();
	    lex.put(id, new Lexion(id, keyword));
	    invLex.put(keyword, id);
	    return id;
	}
	
	/**
	 * Adds a user and returns an ID
	 */
	public int addUser(String username, String password, String firstName, String lastName) {
	    int id = user.size();
	    user.put(username, new User(id, username, password, firstName, lastName));
	    return id;
	}
	
	/**
	 * Tries to log in the user, or else throws a HaltException
	 */
	public boolean getSessionForUser(String username, String password) {
	    if (user.containsKey(username) && user.get(username).getPassword().equals(password))
	       return true;
	    else
	       return false;
	}
	
	/**
	 * Retrieves a document's contents by URL
	 */
	public String getDocument(String url) {
	    if (invUrls.containsKey(url)) {
	        return corpus.get(invUrls.get(url)).getContent();
	    } else
	       return null;
	}
	
	/**
	 * Shuts down / flushes / closes the storage system
	 */
	@Override
	public void close() throws DatabaseException {
        userDb.close();
        corpusDb.close();
        urlsDb.close();
        invDb.close();
        lexDb.close();
        invLexDb.close();
        javaCatalog.close();
        env.close();
	}
}
