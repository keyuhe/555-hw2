package edu.upenn.cis.cis455.model;

import java.io.Serializable;

public class Lexion implements Serializable{
    Integer id;
    String word;
    
    public Lexion(Integer id, String word) {
        this.id = id;
        this.word = word;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Corpus))
            return false;
        
        return ((Lexion) o).getId().equals(this.getId());
    }
}
