package edu.upenn.cis.cis455.model;

import java.io.Serializable;

public class Corpus implements Serializable{
    private Integer id;
    private String documentContents;
    
    public Corpus(int id, String documentContents) {
        this.id = id;
        this.documentContents = documentContents;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public String getContent() {
        return this.documentContents;
    }
    
    public void setContent(String content) {
        this.documentContents = content;
    }
    
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Corpus))
            return false;
        
        return ((Corpus) o).getId().equals(this.getId());
    }
}
