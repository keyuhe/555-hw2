package edu.upenn.cis.cis455.model;

import java.io.Serializable;

public class Url implements Serializable{
    private String url;
    private Integer id;
    private String lastModified;
    
    public Url(Integer id, String url, String lastModified) {
        this.id = id;
        this.url = url;
        this.lastModified = lastModified;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public String getUrl() {
        return this.url;
    }
    
    public String getLastModified() {
        return this.lastModified;
    }
    
    public void setModified(String s) {
        this.lastModified = s;
    }
    
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Corpus))
            return false;
        
        return ((Url) o).getId().equals(this.getId());
    }
}
